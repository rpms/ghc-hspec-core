# generated by cabal-rpm-0.12.1
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name hspec-core
%global pkgver %{pkg_name}-%{version}

%bcond_with tests

Name:           ghc-%{pkg_name}
Version:        2.4.4
Release:        3%{?dist}
Summary:        A Testing Framework for Haskell

License:        MIT
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkgver}/%{pkgver}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  ghc-HUnit-devel
BuildRequires:  ghc-QuickCheck-devel
BuildRequires:  ghc-ansi-terminal-devel
BuildRequires:  ghc-array-devel
BuildRequires:  ghc-async-devel
BuildRequires:  ghc-call-stack-devel
BuildRequires:  ghc-deepseq-devel
BuildRequires:  ghc-directory-devel
BuildRequires:  ghc-filepath-devel
BuildRequires:  ghc-hspec-expectations-devel
BuildRequires:  ghc-quickcheck-io-devel
BuildRequires:  ghc-random-devel
BuildRequires:  ghc-setenv-devel
BuildRequires:  ghc-tf-random-devel
BuildRequires:  ghc-time-devel
BuildRequires:  ghc-transformers-devel
%if %{with tests}
BuildRequires:  ghc-hspec-meta-devel
BuildRequires:  ghc-process-devel
BuildRequires:  ghc-silently-devel
BuildRequires:  ghc-temporary-devel
%endif
# End cabal-rpm deps

%description
This package exposes internal types and functions that can be used to extend
Hspec's functionality.


%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
%if %{defined ghc_version}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
%endif
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development files.


%package devel-doc
Summary:        Haskell %{pkg_name} library development documentation
BuildArch:      noarch

%description devel-doc
This package provides the Haskell %{pkg_name} development documentation.

%prep
%setup -q -n %{pkgver}


%build
%ghc_lib_build


%install
%ghc_lib_install
grep -v "%{_docdir}/ghc/html/libraries/%{pkgver}" %{name}-devel.files > %{name}-devel-nodoc.files
grep "%{_docdir}/ghc/html/libraries/%{pkgver}" %{name}-devel.files > %{name}-devel-doc.files


%check
%cabal_test


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel-nodoc.files

%files devel-doc -f %{name}-devel-doc.files


%changelog
* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 2.4.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Fri Jan 26 2018 Jens Petersen <petersen@redhat.com> - 2.4.4-2
- rebuild

* Fri Nov 10 2017 David Shea <dshea@redhat.com> - 2.4.4-2
- spec file generated by cabal-rpm-0.11.1
- Split API docs into a separate package
